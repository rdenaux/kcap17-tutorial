# [Hybrid techniques for knowledge-based NLP](http://expertsystemlab.com/kcap2017/)

This repo contains [Jupyter](http://jupyter.org/) notebooks, data and supporting libraries to be used at the "Hybrid techniques for knowledge-based NLP" tutorial to be held at K-CAP 2017.

## Set up
To execute this notebook you will need to have a python 3 environment along with various libraries such as [TensorFlow](https://www.tensorflow.org/install/), numpy, etc. 
You have a few options:
 - if you already have an environment set up with Python3, Jupyter notebook and TensorFlow, you can just launch a jupyter notebook that has access to the contents of this repo.
 - otherwise, we recommend installing [Docker](https://docs.docker.com/engine/installation/)
 
### Using Docker
Once you have installed Docker, you can launch a container which provides a Jupyter notebook with TensorFlow and clones this repo. To launch the container, open a terminal (if you used the **Docker Toolbox** in an older Windows machine, use the *Docker QuickStart Terminal*) and type:

    docker run -it -p 9999:8888 rdenaux/kcap17-tutorial:0.3

The first time you run this, docker will download all images required to run the container, which can take a few minutes.

Once the container is launched, you should see a message like:

    Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://localhost:8888/?token=868b5856e07114ff2352375429da29032537d30d4b581921

This means your container is ready and you can open a browser and go to [http://localhost:9999]. (if you are using *Docker Toolbox* use [http://192.168.99.100:9999]) to log into Jupyter. Copy the string after `token` in the message to login.





